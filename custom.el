(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(custom-safe-themes
   '("8f97d5ec8a774485296e366fdde6ff5589cf9e319a584b845b6f7fa788c9fa9a" "a94f1a015878c5f00afab321e4fef124b2fc3b823c8ddd89d360d710fc2bddfc" default))
 '(doom-dracula-brighter-modeline t)
 '(doom-dracula-colorful-headers t)
 '(fci-rule-color "#555556")
 '(helm-autoresize-mode t)
 '(helm-completion-style 'helm-fuzzy)
 '(inhibit-startup-screen t)
 '(jdee-db-active-breakpoint-face-colors (cons "#1B2229" "#fd971f"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1B2229" "#b6e63e"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1B2229" "#525254"))
 '(nrepl-message-colors
   '("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3"))
 '(objed-cursor-color "#e74c3c")
 '(org-format-latex-options
   '(:foreground default :background default :scale 2.0 :html-foreground "Black" :html-background "Transparent" :html-scale 1.0 :matchers
				 ("begin" "$1" "$" "$$" "\\(" "\\[")))
 '(org-preview-latex-default-process 'imagemagick)
 '(org-sticky-header-full-path 'full)
 '(package-archives
   '(("melpa" . "https://melpa.org/packages/")
	 ("gnu" . "https://elpa.gnu.org/packages/")))
 '(package-selected-packages
   '(pinentry sublimity zenburn-theme zenburn helm-posframe focus writeroom-mode vterm-toggle toc-org eyebrowse ivy-rich ox-hugo emmet-mode aggressive-indent ivy-yasnippet yasnippet treemacs-projectile counsel-projectile projectile flycheck nov leuven-theme leuven peg org-sidebar org fancy-battery helm-org-rifle ivy-posframe bar-cursor flyspell-popup god-mode git-gutter+ rainbow-delimiters keyfreq spaceline-all-the-icons exwm helm pdf-tools ace-link all-the-icons-dired highlight-indent-guides ivy-mpdel mpdel solaire-mode auto-package-update swiper all-the-icons-ivy ivy theme-looper centaur-tabs ace-window auctex-latexmk counsel vterm doom-modeline spaceline zerodark-theme auto-complete fzf evil-magit visual-fill-column use-package org-bullets neotree markdown-mode magit evil doom-themes auctex airline-themes))
 '(pdf-view-midnight-colors '("#DCDCCC" . "#383838"))
 '(safe-local-variable-values '((org-agenda-files ".")))
 '(show-paren-mode t)
 '(vc-annotate-background "#1c1e1f")
 '(vc-annotate-color-map
   (list
	(cons 20 "#b6e63e")
	(cons 40 "#c4db4e")
	(cons 60 "#d3d15f")
	(cons 80 "#e2c770")
	(cons 100 "#ebb755")
	(cons 120 "#f3a73a")
	(cons 140 "#fd971f")
	(cons 160 "#fc723b")
	(cons 180 "#fb4d57")
	(cons 200 "#fb2874")
	(cons 220 "#f43461")
	(cons 240 "#ed404e")
	(cons 260 "#e74c3c")
	(cons 280 "#c14d41")
	(cons 300 "#9c4f48")
	(cons 320 "#77504e")
	(cons 340 "#555556")
	(cons 360 "#555556")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(all-the-icons-dired-dir-face ((t `(:foreground ,(face-background 'default)))))
 '(fixed-pitch ((t (:family "Iosevka SS08"))))
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-code ((t (:inherit fixed-pitch))))
 '(org-footnote ((t (:inherit fixed-pitch))))
 '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
 '(org-table ((t (:inherit fixed-pitch))))
 '(org-verbatim ((t (:inherit fixed-pitch))))
 '(variable-pitch ((t (:family "Fira Sans")))))
